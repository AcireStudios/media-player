///////////////////////////////////////////////////////////
//  PlayerView.as
//  ActionScript 3.0 Implementation of the Class PlayerView
//  Original author: Erica Stanley
///////////////////////////////////////////////////////////

package com.kingdomtools.mediaplayer.view
{
	import com.kingdomtools.mediaplayer.controller.player.*;
	
	import flash.display.MovieClip;

	/**
	 * Singleton. The PlayerView class acts as a connector between the player related view
	 * objects in the theme swf and the PlayerController functionality.  It also holds
	 * information needed to display the current view state of the player (compact,
	 * normal, full screen).
	 * @author Erica Stanley
	 * @version 1.0
	 * @created 01-Mar-2010 4:18:31 PM
	 */
	public class PlayerView
	{
	    /**
	     * holds aspect ratio of media currently displayed in the player view {width:
	     * Number, height:Number}
	     */
	    protected var _aspectRatio: Object;
	    /**
	     * This is the controller for the player.  It follows the factory pattern and
	     * selects the appropriate player for media type.
	     */
	    private var _controller: ProgressiveVideoPlayer;
	    /**
	     * Playlist Controller allows user to skip to previous or next media item from
	     * within the player view.
	     */
	    private var _listController: Object;
	    /**
	     * Indicates whether player should maintain the current aspect ratio or fit the
	     * media to the display area; Default value is true
	     */
	    public var maintainAspectRatio: Boolean = true;
	    /**
	     * Indicates the current view of the player, either "compact", "normal" and
	     * "fullScreen"
	     */
	  	public var controls:MovieClip;
	    /**
	     *Indicates the current view state: "compact"|"normal"|"fullScreen" 
	     */	     
	    public var viewState: String;
		/**
		 *Indicates whether the player should maintain the aspect ratio calculated from the metadata's width and height
		 */	    
		 
		 //Nested MovieClips
		 public var controls:MovieClip;
		 public var vidDisplay:Video;
		 public var back:MovieClip;
		 
		 
		public var maintainAspectRatio: Boolean = true;
	    /**
	     * Constructor.  Initializes the controller and add event listeners to controls
	     */
		public function PlayerView(){
			_controller = new ProgressiveVideoPlayer();
			_controller.playerView = this;
			
			// add event listeners to all buttons
			controls.btnPause.addEventListener(MouseEvent.CLICK, _controller.pauseClicked);
			controls.btnPlay.addEventListener(MouseEvent.CLICK, _controller.playClicked);
			controls.btnStop.addEventListener(MouseEvent.CLICK, _controller.stopClicked);
			controls.btnMute.addEventListener(MouseEvent.CLICK, _controller.muteClicked);
			controls.btnUnmute.addEventListener(MouseEvent.CLICK,_controller.unmuteClicked);
			controls.btnMenu.addEventListener(MouseEvent.CLICK,_controller.toggleMenu);
			
			/* controls.progressbar.buttonMode = true;
			controls.progressbar.addEventListener(MouseEvent.CLICK, _controller.progressbarClicked); */
			
			
			controls.btnMaximize.addEventListener(MouseEvent.CLICK, _controller.fullscreenOnClicked);
			controls.btnMinimize.addEventListener(MouseEvent.CLICK, _controller.fullscreenOffClicked);

		}

	    /**
	     * Sets the aspect ratio for the current media in the player
	     * 
	     * @param w
	     * @param h
	     */
	    public function setAspectRatio(w:Number, h:Number): void
	    {
	    }

	}//end PlayerView

}