///////////////////////////////////////////////////////////
//  AudioPlayerController.as
//  ActionScript 3.0 Implementation of the Class AudioPlayerController
//  Original author: Erica Stanley
///////////////////////////////////////////////////////////

package com.kingdomtools.mediaplayer.controller.player
{

	import com.kingdomtools.mediaplayer.controller.player.IPlayer;
	import com.kingdomtools.mediaplayer.controller.player.PlayerController;
	/**
	 * This is the concrete AudioPlayerFactory class.
	 * @author Erica Stanley
	 * @version 1.0
	 * @created 01-Mar-2010 4:18:30 PM
	 */
	public class AudioPlayerController extends PlayerController
	{
		public function AudioPlayerController(){

		}

	    /**
	     * Abstract method.  Must be overridden in a specialized player factory subclass.
	     * Returns null.
	     */
	   override protected function createPlayerController(): IPlayer
	    {
	    	return null;
	    }

	}//end AudioPlayerController

}