///////////////////////////////////////////////////////////
//  PlayerController.as
//  ActionScript 3.0 Implementation of the Class PlayerController
//  Original author: Erica Stanley
///////////////////////////////////////////////////////////

package com.kingdomtools.mediaplayer.controller.player
{
	import com.kingdomtools.mediaplayer.controller.player.IPlayer;

	import com.kingdomtools.mediaplayer.controller.player.IPlayer;
	/**
	 * This is the abstract class that defers control to the appropriate player based
	 * on media type ("audio" "video".)
	 * @author Erica Stanley
	 * @version 1.0
	 * @created 01-Mar-2010 4:18:30 PM
	 */
	public class PlayerController
	{
		public function PlayerController(){

		}

	    /**
	     * Abstract method.  Must be overridden in a specialized player factory subclass.
	     * Returns null.
	     */
	    protected function createPlayerController(): IPlayer
	    {
	    	return null;
	    }

	}//end PlayerController

}