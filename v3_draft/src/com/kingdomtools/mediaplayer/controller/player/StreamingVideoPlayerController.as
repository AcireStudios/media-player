///////////////////////////////////////////////////////////
//  StreamingVideoPlayerController.as
//  ActionScript 3.0 Implementation of the Class StreamingVideoPlayerController
//  Original author: Erica Stanley
///////////////////////////////////////////////////////////

package com.kingdomtools.mediaplayer.controller.player
{
	import com.kingdomtools.mediaplayer.controller.player.IPlayer;
	import com.kingdomtools.mediaplayer.controller.player.PlayerController;

	/**
	 * This is the concrete StreamingVideoPlayerFactory.
	 * @author Erica Stanley
	 * @version 1.0
	 * @created 01-Mar-2010 4:18:31 PM
	 */
	public class StreamingVideoPlayerController extends PlayerController
	{
		public function StreamingVideoPlayerController(){

		}

	    /**
	     * This function overrides the createPlayerController function in the
	     * PlayerFactory base class.  Returns a reference to a StreamingVideoPlayer.
	     */
	    override protected function createPlayerController(): IPlayer
	    {
	    	return null;
	    }

	}//end StreamingVideoPlayerController

}