///////////////////////////////////////////////////////////
//  IPlayer.as
//  ActionScript 3.0 Implementation of the Interface IPlayer
//  Original author: Erica Stanley
///////////////////////////////////////////////////////////

package com.kingdomtools.mediaplayer.controller.player
{
	import com.kingdomtools.mediaplayer.model.MediaItemModel;

	import com.kingdomtools.mediaplayer.model.MediaItemModel;
	/**
	 * Interface implemented by all players
	 * @author Erica Stanley
	 * @version 1.0
	 * @created 01-Mar-2010 4:18:30 PM
	 */
	public interface IPlayer
	{
		/**
		 * Initializes the player, opens the media stream and collects metadata
		 */
		function init(): void;

		/**
		 * Triggered when player enters full screen mode.
		 */
		function onEnterFullScreen(): void;

		/**
		 * Triggered when user stops dragging the scubber or when releasing the left mouse
		 * button any where on the progress bar.
		 */
		function onScrubEnded(): void;

		/**
		 * Triggered when user starts to drag the scubber or when pressing the left mouse
		 * button any where on the progress bar.
		 */
		function onScrubStarted(): void;

		/**
		 * Plays the current media source
		 */
		function play(): void;

		/**
		 * Seek to desired time.  If media was playing before seeking was initiated, media
		 * will resume playback from seek time.  If media was paused or stopped, media
		 * will pause at seek time.
		 */
		function seek(): void;

		/**
		 * Stops playback and closes stream to current media source.
		 */
		function stop(): void;

		/**
		 * Toggles between normal and full screen modes.
		 */
		function toggleFullScreen(): void;

		/**
		 * Play/ pause toggle functionality.
		 */
		function togglePause(): void;
	}//end IPlayer

}