///////////////////////////////////////////////////////////
//  AudioPlayer.as
//  ActionScript 3.0 Implementation of the Class AudioPlayer
//  Original author: Erica Stanley
///////////////////////////////////////////////////////////

package com.kingdomtools.mediaplayer.controller.player
{
	import com.kingdomtools.mediaplayer.controller.player.IPlayer;

	import com.kingdomtools.mediaplayer.controller.player.IPlayer;
	/**
	 * This is the concrete class that controls media playback of type "audio."
	 * @author Erica Stanley
	 * @version 1.0
	 * @created 01-Mar-2010 4:18:29 PM
	 */
	public class AudioPlayer
	{
		public function AudioPlayer(){

		}

	}//end AudioPlayer

}