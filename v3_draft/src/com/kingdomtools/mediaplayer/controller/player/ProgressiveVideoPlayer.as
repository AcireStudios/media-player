///////////////////////////////////////////////////////////
//  ProgressiveVideoPlayer.as
//  ActionScript 3.0 Implementation of the Class ProgressiveVideoPlayer
//  Original author: Erica Stanley
///////////////////////////////////////////////////////////

package com.kingdomtools.mediaplayer.controller.player
{
	import com.kingdomtools.mediaplayer.controller.player.VideoPlayer;

	import com.kingdomtools.mediaplayer.controller.player.VideoPlayer;
	/**
	 * This is the concrete class that is responsible for media control when the media
	 * type is progressive video.
	 * @author Erica Stanley
	 * @version 1.0
	 * @created 01-Mar-2010 4:18:31 PM
	 */
	public class ProgressiveVideoPlayer extends VideoPlayer
	{
		public function ProgressiveVideoPlayer(){

		}

	}//end ProgressiveVideoPlayer

}