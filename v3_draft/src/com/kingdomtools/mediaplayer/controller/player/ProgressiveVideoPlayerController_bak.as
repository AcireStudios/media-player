///////////////////////////////////////////////////////////
//  ProgressiveVideoPlayerController.as
//  ActionScript 3.0 Implementation of the Class ProgressiveVideoPlayerController
//  Original author: Erica Stanley
///////////////////////////////////////////////////////////

package com.kingdomtools.mediaplayer.controller.player
{

	import com.kingdomtools.mediaplayer.view.PlayerView;
	
	import flash.events.MouseEvent;
	import flash.geom.*;
	
	/**
	 * This is the concrete ProgressiveVideoPlayerFactory.
	 * @author Erica Stanley
	 * @version 1.0
	 * @created 01-Mar-2010 4:18:31 PM
	 */
	public class ProgressiveVideoPlayerController_bak extends PlayerController
	{

		/**
	     * Time to buffer for the video in seconds.
	     */
		public const BUFFER_TIME:Number	= 8;
		/**
	     * Start volume when initializing player
	     */
		public const DEFAULT_VOLUME:Number	= 0.6;
		/**
	     * Update delay in milliseconds.
	     */
		public const DISPLAY_TIMER_UPDATE_DELAY:int	= 10;
		/**
		 * Smoothing for video. may slow down old computers
		 */		
		public const SMOOTHING:Boolean	= true;
		/**
		 * Flag indicating flv has been loaded
		 */		
		public var bolLoaded:Boolean = false;
		/**
		 * Flag for volume scrubbing
		 */		
		public var bolVolumeScrub:Boolean = false;
		/**
		 * Flag for progress scrubbing
		 */		
		public var bolProgressScrub:Boolean	= false;
		/**
		 * Holds the last used volume, but never 0
		 */		
		public var intLastVolume:Number				= DEFAULT_VOLUME;
		/**
		 *Net connection object for net stream 
		 */		
		public var ncConnection:NetConnection;
		/**
		 *Net stream object. 
		 */	
		public var nsStream:NetStream;
		/**
		 *Object holds all meta data 
		 */		
		public var objInfo:Object;
		/**
		 *url to current video 
		 */		
		public var strSource:String; // MovieClip(this.parent).currentVideoURL;
		// timer for updating player (progress, volume...)
		public var tmrDisplay:Timer;
		
		public var aR:Object;
		
		public var minVideoWidth:Number;
		public var minVideoHeight:Number;
		
		public var videoAreaHeight:Number; //= stage.stageHeight - 80;
		public var videoAreaWidth:Number; //= stage.stageWidth - 4;
		
		public var preloader:Sprite;
		public var preloaderArr:Array = new Array();
		public var playCount:Number;
		/**
		 *Reference to the player view singleton
		 */		
		public var playerView:PlayerView;
		
		
		/**
		 *Constructor.  Calls init(). 
		 * 
		 */		
		public function ProgressiveVideoPlayerController(){
			init();
		}

	    /**
	     * Abstract method.  Must be overridden in a specialized player factory subclass.
	     * Returns null.
	     */
	    override protected function createPlayerController(): IPlayer
	    {
	    	return null;
	    }
	    
	    


	/**
	 *Initializes the player. 
	 * 
	 */	
	public function init():void {
		//set up video area
		
		// hide buttons
		menuBtn.visible = false;
		playerView.controls.btnUnmute.visible	= false;
		playerView.controls.btnPause.visible	= false;
		playerView.controls.btnMinimize.visible	= false;
		
		
		playerView.controls.progressbar.timeFill.width = 1;
		playerView.controls.progressbar.loadFill.width = 1;
		
		// add global event listener when mouse is released
		//stage.addEventListener( MouseEvent.MOUSE_UP, mouseReleased);
		
		// add fullscreen listener
		stage.addEventListener(FullScreenEvent.FULL_SCREEN, onFullscreen);
		
		// create timer for updating all visual parts of player and add
		// event listener
		tmrDisplay = new Timer(DISPLAY_TIMER_UPDATE_DELAY);
		tmrDisplay.addEventListener(TimerEvent.TIMER, updateDisplay);
		
		// create a new net connection, add event listener and connect
		// to null because we don't have a media server
		ncConnection = new NetConnection();
		ncConnection.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
		ncConnection.connect(null);
		
		// create a new netstream with the net connection, add event
		// listener, set client to this for handling meta data and
		// set the buffer time to the value from the constant
		nsStream = new NetStream(ncConnection);
		nsStream.addEventListener(NetStatusEvent.NET_STATUS, netStatusHandler);
		nsStream.client = this;
		nsStream.bufferTime = BUFFER_TIME;
		
		// attach net stream to video object on the stage
		vidDisplay.attachNetStream(nsStream);
		// set the smoothing value from the constant
		vidDisplay.smoothing = SMOOTHING;
	
		// set default volume
		setVolume(DEFAULT_VOLUME);
		
		playCount = 0;
	}
	/**
	 *Event Listener for play button
	 * @param e
	 * 
	 */	
	public function playClicked(e:MouseEvent):void {
		// check's, if the flv has already begun
		// to download. if so, resume playback, else
		// load the file
		trace("playClicked");
		if(!bolLoaded) {
			trace("playing: "+ strSource);
			nsStream.play( MovieClip(this.parent).currentVideoURL );
			//bolLoaded = true;
		}
		else{
			nsStream.resume();
		}
		
		// show video display
		vidDisplay.visible					= true;
		
		// switch play/pause visibility
		playerView.controls.btnPause.visible	= true;
		playerView.controls.btnPlay.visible		= false;
	}
	/**
	 *Event Listener for  pause button
	 * @param e
	 * 
	 */	
	public function pauseClicked(e:MouseEvent):void {
		// pause video
		nsStream.pause();
		
		// switch play/pause visibility
		playerView.controls.btnPause.visible	= false;
		playerView.controls.btnPlay.visible		= true;
	}
	/**
	 *Event Listener for mute button 
	 * @param e
	 * 
	 */	
	public function muteClicked(e:MouseEvent):void {
		// set volume to 0
		setVolume(0);
		
	}
	/**
	 *Event Listener for unmute button
	 * @param e
	 * 
	 */	
	public function unmuteClicked(e:MouseEvent):void {
		// set volume to last used value
		setVolume(intLastVolume);
	
	}
	
	/**
	 *Event Listener for clicking on the progress bar 
	 * @param e
	 * 
	 */	
	public function progressbarClicked(e:MouseEvent):void{
		var percentage = playerView.controls.progressbar.mouseX / playerView.controls.progressbar.track.width;
		var newSeekTo = objInfo.duration * percentage;
		nsStream.seek(Math.round(newSeekTo));
		
	}
	/**
	 *Updates the progress bar for the current time 
	 * @param e
	 * 
	 */	
	public function updateDisplay(e:TimerEvent):void {
		
		playerView.controls.progressbar.timeFill.width = nsStream.time * playerView.controls.progressbar.track.width / objInfo.duration
		playerView.controls.progressbar.loadFill.width	= nsStream.bytesLoaded * playerView.controls.progressbar.track.width / nsStream.bytesTotal;
		
		
	}
	/**
	 *Function to set size of the player module 
	 * @param w
	 * @param h
	 * 
	 */	
	public function setSize(w:Number, h:Number):void{
		back.width = w;
		back.height = h;
		
		playerView.controls.setWidth(w);
		playerView.controls.y = (back.y + back.height) - playerView.controls.back.height;
	}
	
	/**
	 *Function called after metaData received. 
	 * @param infoObject
	 * 
	 */	
	public function onMetaData(infoObject:Object):void {
		if(preloaderArr[playCount] != undefined){
				this.removeChild(preloaderArr[playCount]);
				playCount++;
			}
		
		// stores meta data in a object
		objInfo = infoObject;
		//this.back.height = this.videoAreaHeight;
		// now we can start the timer because
		// we have all the neccesary data
		tmrDisplay.start();
		
		var key:String;
	    for (key in infoObject)
	    {
	        trace(key + ": " + infoObject[key]);
	    }
		
		trace("vid loaded?"+bolLoaded);
		
		if(!bolLoaded){
			aR = getAspectRatio(objInfo.width, objInfo.height);
			
			//var newWidth = stage.stageWidth - 2;
			this.minVideoWidth = videoAreaWidth;
			trace("----width of stage: "+ stage.stageWidth);
			
			//var newHeight = getOptimalHeightForWidth(newWidth, aR.width, aR.height);
			this.minVideoHeight = getOptimalHeightForWidth(this.minVideoWidth, aR.width, aR.height);
			
			trace("new dimensions: " + this.minVideoWidth +"x"+ this.minVideoHeight);
			
			if(stage.displayState == StageDisplayState.NORMAL){
		
				this.back.height = this.videoAreaHeight;
				
				this.vidDisplay.width = this.minVideoWidth;
				this.vidDisplay.height = Math.round(this.minVideoHeight);
				
				//back.y = 0;
			
			
				this.vidDisplay.x = 0;
				this.vidDisplay.y = (this.back.height - this.vidDisplay.height)/2;
				
				playerView.controls.y =  vidDisplay.y + (vidDisplay.height - playerView.controls.height);
			}else{
				
				var fullScreen_width = Capabilities.screenResolutionX;
				var fullScreen_height = getOptimalHeightForWidth(fullScreen_width, aR.width, aR.height);
				
				vidDisplay.width = fullScreen_width;
				vidDisplay.height = fullScreen_height;
				
				vidDisplay.x=0;
				vidDisplay.y= (Capabilities.screenResolutionY - vidDisplay.height)/2 -52;
				
				playerView.controls.y =  vidDisplay.y + (vidDisplay.height - playerView.controls.height) -25;
				playerView.controls.x =  vidDisplay.x + ((vidDisplay.width - playerView.controls.width)/2);
			}
		}
		vidDisplay.visible = true;
		bolLoaded = true;
		
	
	}
	/**
	 *This function handles Net Status Events
	 * @param event
	 * 
	 */	
	public function netStatusHandler(event:NetStatusEvent):void {
		// handles net status events
		switch (event.info.code) {
			// trace a messeage when the stream is not found
			case "NetStream.Play.StreamNotFound":
				trace("Stream not found: " + strSource);
			break;
			
			// when the video reaches its end, we stop the player
			case "NetStream.Play.Stop":
				stop();
			break;
		}
	}
	
	public function getAspectRatio(w:Number, h:Number):Object{
		//return object to hold aspect ratio
		var aspectRatio = new Object();
		
		//find greatest common factor
		var minDimension= Math.min(w, h);
		var tmpFactor1 = 0;
		var tmpFactor2 = 0;
		var gcf = 0;
		var remainder1= 0;
		var remainder2 = 0;
		
		
		for(var i = 1; gcf == 0; i++){
			tmpFactor1 = minDimension/i;
			remainder1 = tmpFactor1 - Math.floor(tmpFactor1);
			if(remainder1 == 0 ){
				minDimension == w ? tmpFactor2 = h/tmpFactor1 : tmpFactor2 = w/tmpFactor1;
				remainder2 = tmpFactor2 - Math.floor(tmpFactor2);
				if (remainder2 == 0){
					gcf= tmpFactor1;
				}
			}
	
		}
		//divide w by gcm ->aspectWidth
		aspectRatio.width = w/gcf;
		//divide h by gcm -> aspectHeight
		aspectRatio.height = h/gcf;
		
		trace("----Aspect Ratio: "+ aspectRatio.width +":"+ aspectRatio.height);
		
		return aspectRatio;
		
	}
	/**
	 * This function uses the width and the aspect ratio to return the optimal height (Number) for the specified width.
	 * @param w
	 * @param aRatio_width
	 * @param aRatio_height
	 * @return 
	 * 
	 */	
	public function getOptimalHeightForWidth(w:Number, aRatio_width:Number, aRatio_height:Number):Number{
		
		var gcf = w/aRatio_width;
		return aRatio_height * gcf;
		//return newVidHeight + this.current_controls.btnStop._height;
		
	}
	/**
	 * This function uses the height and the aspect ratio to return the optimal width (Number) for the specified height.
	 * @param h
	 * @param aRatio_width
	 * @param aRatio_height
	 * @return 
	 * 
	 */		
	public function getOptimalWidthForHeight(h:Number, aRatio_width:Number, aRatio_height:Number):Number{
		
		//h= h - this.current_controls.btnStop._height;//take height of control bar into account
		var gcf = h/aRatio_height;
		return aRatio_width * gcf;
		
	}
	/**
	 *This function plays the current video.  It starts the preloader for buffering, which is removed when we get the play event back from the net stream events.
	 * @param bolPlay
	 * 
	 */	
	public function play(bolPlay = true):void {
		if(bolPlay) {
			// stop timer
			//tmrDisplay.stop();
			
			// hide video display
			vidDisplay.visible					= false;
			
			if(this.preloaderArr[playCount] != undefined){
				this.removeChild(preloaderArr[playCount]);
				playCount++;
			}
			
			//preloaderArr[playCount] = new CircleSlicePreloader();
			//preloader = preloaderSprite;
			preloaderArr[playCount] = new Sprite();
			preloaderArr[playCount] = this.addChild( new CircleSlicePreloader());
			preloaderArr[playCount].x = (back.width - preloaderArr[playCount].width)/2;
			preloaderArr[playCount].y = back.y + (back.height - preloaderArr[playCount].height)/2;
			
			
			// play requested video
			nsStream.play( MovieClip(this.parent).currentVideoURL );
			
			// switch button visibility
			playerView.controls.btnPause.visible	= true;
			playerView.controls.btnPlay.visible		= false;
		} else {
			strSource =  MovieClip(this.parent).currentVideoURL;
		}
		
		
	}
	
	/**
	 *Stops the current video. 
	 * 
	 */	
	public function stop():void {
		// pause netstream, set time position to zero
		nsStream.pause();
		nsStream.seek(0);
		
		
		// switch play/pause button visibility
		playerView.controls.btnPause.visible	= false;
		playerView.controls.btnPlay.visible		= true;
		
		//desired onComplete functionality
		
	}
	/**
	 *Sets volume to specified value. 
	 * @param intVolume
	 * 
	 */	
	public function setVolume(intVolume:Number = 0):void {
		// create soundtransform object with the volume from
		// the parameter
		var sndTransform		= new SoundTransform(intVolume);
		// assign object to netstream sound transform object
		nsStream.soundTransform	= sndTransform;
		
		// hides/shows mute and unmute button according to the
		// volume
		if(intVolume > 0) {
			playerView.controls.btnMute.visible		= true;
			playerView.controls.btnUnmute.visible	= false;
		} else {
			playerView.controls.btnMute.visible		= false;
			playerView.controls.btnUnmute.visible	= true;
		}
	}
	/**
	 *Event Listener for maximize button 
	 * @param e
	 * 
	 */	
	public function fullscreenOnClicked(e:MouseEvent):void {
		trace("maximize clicked");
		
		// go to fullscreen mode
		vidDisplay.width = objInfo.width;
		vidDisplay.height = objInfo.height;
		
		trace("my resolution: " +Capabilities.screenResolutionX +"x"+ Capabilities.screenResolutionY);
		
		var y_offset =vidDisplay.y - back.y;
		
		//stage.fullScreenSourceRect = new Rectangle(vidDisplay.x, vidDisplay.y + y_offset, vidDisplay.x + vidDisplay.width, vidDisplay.y + vidDisplay.height);
		//stage.displayState = StageDisplayState.FULL_SCREEN;
		
		//control scaling workaround/////////////////////
		stage.fullScreenSourceRect = new Rectangle(0, 0, Capabilities.screenResolutionX, Capabilities.screenResolutionY);
		stage.displayState = StageDisplayState.FULL_SCREEN;
		
		var fullScreen_width = Capabilities.screenResolutionX;
		var fullScreen_height = getOptimalHeightForWidth(fullScreen_width, aR.width, aR.height);
		
		vidDisplay.width = fullScreen_width;
		vidDisplay.height = fullScreen_height;
		
		vidDisplay.x=0;
		vidDisplay.y= (Capabilities.screenResolutionY - vidDisplay.height)/2 -52;
		
		playerView.controls.y =  vidDisplay.y + (vidDisplay.height - playerView.controls.height) -25;
		playerView.controls.x =  vidDisplay.x + ((vidDisplay.width - playerView.controls.width)/2);
		
		/////////////////
		
	}
	/**
	 *Event Listener for minimize button 
	 * @param e
	 * 
	 */	
	public function fullscreenOffClicked(e:MouseEvent):void {
		trace("minimize clicked");
		
		// go to back to normal mode
		stage.displayState = StageDisplayState.NORMAL;
	}
	/**
	 *Called when either entering or exiting full screen 
	 * @param e
	 * 
	 */	
	public function onFullscreen(e:FullScreenEvent):void {
		// check if we're entering or leaving fullscreen mode
	    if (e.fullScreen) {
			
			
			// switch fullscreen buttons
	        playerView.controls.btnMaximize.visible = false;
			playerView.controls.btnMinimize.visible = true;
			playerView.controls.btnMenu.visible = true;
			
			 playerView.controls.back.gotoAndStop(2);
			 playerView.controls.setWidth(400);
			 /*playerView.controls.y =  vidDisplay.y + (vidDisplay.height - playerView.controls.height);
			 playerView.controls.x =  vidDisplay.x + ((vidDisplay.width - playerView.controls.width)/2);*/
			
			back.x = 0;
			back.y = -52;
			back.width = Capabilities.screenResolutionX;
			back.height = Capabilities.screenResolutionY;
			
			/*MovieClip(this.parent).banner.alpha = 0;
			MovieClip(this.parent).chrome.alpha = 0;
			MovieClip(this.parent).footer.alpha = 0;*/
			
			
			MovieClip(this.parent).btnShare.alpha = 0;
			MovieClip(this.parent).playerMenu.setSize(255, 230);
			
			
			
			
			
	    } else {
			back.x = 0;
			back.y = 1;
			back.width = stage.width -3;
			back.height = videoAreaHeight + 2;
			
			vidDisplay.width = this.minVideoWidth;
			vidDisplay.height = this.minVideoHeight;
			
			
			
			vidDisplay.x=2;
			vidDisplay.y= (back.height - vidDisplay.height)/2;
			
			
			
			// switch fullscreen buttons
	        playerView.controls.btnMaximize.visible = true;
			playerView.controls.btnMinimize.visible = false;
			
			playerView.controls.back.gotoAndStop(1);
			playerView.controls.setWidth(this.minVideoWidth);
			playerView.controls.x = 2;
			playerView.controls.y =  vidDisplay.y + (vidDisplay.height - playerView.controls.height);
			
	    }
	}
	
	
	
	}//end ProgressiveVideoPlayerController

}