///////////////////////////////////////////////////////////
//  MediaPlayer.as
//  ActionScript 3.0 Implementation of the Class MediaPlayer
//  Original author: Erica Stanley
///////////////////////////////////////////////////////////

package com.kingdomtools.mediaplayer
{
	/**
	 * This is the main controller for the media player application.  It is
	 * responsible for loading the church specific XML and initializing the
	 * application based on that information.  It loads themes remotely, providing a
	 * preloader until the view is completely loaded.
	 * @author Erica Stanley
	 * @version 1.0
	 * @created 01-Mar-2010 4:18:30 PM
	 */
	public class MediaPlayer
	{
	    /**
	     * Indicates whether application data has been loaded and models properly
	     * instantiated.
	     */
	    private var applicationLoaded: Boolean;
	    private var loadTimer: Object;
	    /**
	     * Object (can be class or swf) to represent the preloader.
	     */
	    public var preloader: Object;
	    /**
	     * Indicates whether the view has been loaded with no error.  On successful load,
	     * preloader is removed.
	     */
	    private var viewLoaded: Boolean;

	    /**
	     * Loads application component with the appropriate XMLLIst.  Returns true on
	     * succesful completion.
	     */
	    public function initApplication(): void
	    {
	    }

	    /**
	     * loads specified Bible version with the given XMLList.
	     */
	    public function loadBible(): void
	    {
	    }

	    /**
	     * loads the playlist and media items with the specified XMLList.
	     * 
	     * @param pList
	     */
	    public function loadPlaylist(pList:XMLList): void
	    {
	    }

	    /**
	     * Loads branding and theme with the data from the specified XML
	     * 
	     * @param skinXML
	     */
	    public function loadSkin(skinXML:XML): void
	    {
	    }

	    /**
	     * Constructor. loads XML and starts preloader.
	     */
	    public function MediaPlayer(): void
	    {
	    }

	    /**
	     * Called when XML has completely loaded and has data ready.  Calls
	     * initApplication() method.
	     */
	    protected function onXMLLoad(): void
	    {
	    }

	}//end MediaPlayer

}