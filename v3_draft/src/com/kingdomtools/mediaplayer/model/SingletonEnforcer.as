///////////////////////////////////////////////////////////
//  SingletonEnforcer.as
//  ActionScript 3.0 Implementation of the Class SingletonEnforcer
//  Original author: Erica Stanley
///////////////////////////////////////////////////////////

package com.kingdomtools.mediaplayer.model
{
	/**
	 * @author Erica Stanley
	 * @version 1.0
	 * @created 02-Feb-2010 01:59:22 AM
	 */
	internal class SingletonEnforcer
	{
		public function SingletonEnforcer(){

		}

	}//end SingletonEnforcer

}