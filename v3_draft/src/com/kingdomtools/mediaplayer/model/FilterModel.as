///////////////////////////////////////////////////////////
//  FilterModel.as
//  ActionScript 3.0 Implementation of the Class FilterModel
//  Original author: Erica Stanley
///////////////////////////////////////////////////////////

package com.kingdomtools.mediaplayer.model
{
	/**
	 * @author Erica Stanley
	 * @version 1.0
	 * @created 02-Feb-2010 01:59:22 AM
	 */
	public class FilterModel
	{
	    /**
	     * An array containing the id of items contained in this filter.
	     */
	    private var _filteredItems: Array;
	    /**
	     * String representing the name of the filter
	     */
	    private var _filterName: String;
	    /**
	     * Number of items contained in this filter.
	     */
	    private var _itemCount: int;

	    /**
		 * Adds specified itemID to the filteredItems Array and increments the itemCount
		 * property.
		 * 
		 * @param itemID
		 */
	    public function addItem(itemID:int): void
	    {
	    }

	    /**
		 * Constructor.  Creates filter with specified name.
		 * 
		 * @param fName
		 */
	    public function FilterModel(fName:String): void
	    {
	    }

	    /**
	     * Read only.  Total number of items contained in this filter.
	     */
	    public function get itemCount(): int
	    {
	    	return _itemCount;
	    }

		
	}//end FilterModel

}