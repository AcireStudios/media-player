///////////////////////////////////////////////////////////
//  BookmarkModel.as
//  ActionScript 3.0 Implementation of the Class BookmarkModel
//  Original author: Erica Stanley
///////////////////////////////////////////////////////////

package com.kingdomtools.mediaplayer.model
{
	/**
	 * This class maintains all relevant data for a bookmark, including:  name,
	 * description, date, and scripture location.
	 * @author Erica Stanley
	 * @version 1.0
	 * @updated 01-Mar-2010 4:18:30 PM
	 */
	public class BookmarkModel
	{
	    /**
	     * Date this bookmark was added.
	     */
	    private var _dateAdded: Date;
	    /**
	     * Brief description of bookmark.
	     */
	    private var _description: String;
	    /**
	     * This is an object indicating the location of the bookmarked scripture, by book,
	     * chapter, and scripture.
	     */
	    private var _location: Object;
	    /**
	     * User specified bookmark name.
	     */
	    private var _name: String;

		public function BookmarkModel(){

		}

	    /**
		 * Constructor.  Creates new bookmark with specified name and description.  Name
		 * is required. Description is optional
		 * 
		 * @param bmName
		 * @param bmDesc
		 */
	    public function Bookmark(bmName:String, bmDesc:String = ""): void
	    {
	    }

	    /**
		 * Sets the location of the bookmark, by book, chapter, and scripture.
		 * 
		 * @param book
		 * @param chapter
		 * @param scripture
		 */
	    public function setLocation(book:String, chapter:int, scripture:int): void
	    {
	    }

	}//end BookmarkModel

}