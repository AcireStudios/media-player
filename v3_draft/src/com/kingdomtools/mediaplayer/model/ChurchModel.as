///////////////////////////////////////////////////////////
//  ChurchModel.as
//  ActionScript 3.0 Implementation of the Class ChurchModel
//  Original author: Erica Stanley
///////////////////////////////////////////////////////////

package com.kingdomtools.mediaplayer.model
{
	/**
	 * This class maintains all data relevant to the Church associated with this
	 * player, including: name, logo URL, description, address and labels.
	 * @author Erica Stanley
	 * @version 1.0
	 * @updated 01-Mar-2010 4:18:30 PM
	 */
	public class ChurchModel
	{
	    /**
	     * Physical address of church. This is displayed and used to click out to a google
	     * map of the church's physical address.
	     */
	    private var _address: String;
	    /**
	     * Church description.
	     */
	    private var _description: String;
	    /**
	     * Name of Church
	     */
	    private var _name: String;
	    /**
	     * An associated array (key-value pairs) of related fields for the church.
	     */
	    private var labels: Array;
	    /**
	     * Specifies the URL to the logo image.
	     */
	    private var logoURL: String;

		public function ChurchModel(){

		}

	}//end ChurchModel

}