///////////////////////////////////////////////////////////
//  PresentationModel.as
//  ActionScript 3.0 Implementation of the Class PresentationModel
//  Original author: Erica Stanley
///////////////////////////////////////////////////////////

package com.kingdomtools.mediaplayer.model
{
	import com.kingdomtools.mediaplayer.model.MediaItemModel;
	import com.kingdomtools.mediaplayer.model.CuePointModel;

	/**
	 * The PresentationModel is a composite of MediaItems and CuePoints.  It maintains
	 * the associated MediaItem, an array of CuePoints, an array of labels and default
	 * labels such as title, publishDate and description.
	 * @author Erica Stanley
	 * @version 1.0
	 * @updated 01-Mar-2010 4:18:31 PM
	 */
	public class PresentationModel
	{
	    /**
	     * This is an array of CuePoints or time-dependent slides associated with this
	     * presentation.  Each element of the array can be of type text, image or swf.
	     */
	    private var _cuePoints: Array;
	    /**
	     * Description for this presentation.
	     */
	    private var _description: String;
	    /**
	     * Title of this presentation.
	     */
	    private var _publishDate: String;
	    /**
	     * Title for this presentation.
	     */
	    private var _title: String;
	    /**
	     * An associated array of labels or tags, specified in key-value pairs, for this
	     * presentation.  An indication of which filters this presentation may belong to.
	     */
	    internal var labels: Array;
	    /**
	     * This is the MediaItem associated with this presentation.  It can be of either
	     * type audio or video.
	     */
	    private var mediaItem: MediaItemModel;

	    /**
		 * Adds a CuePoint to the Presentation's cuePoint array.
		 * 
		 * @param cueObj
		 */
	    public function addCuePoint(cueObj:CuePointModel): void
	    {
	    }

	    /**
		 * Adds a mediaItem to this presentation.
		 * 
		 * @param type
		 * @param srcURL
		 */
	    public function addMedia(type:String, srcURL:String): void
	    {
	    }

	    /**
	     * Returns the descriptions for this presentation.
	     */
	    public function get itemDescription(): String
	    {
	    	return _description;
	    }

	    /**
	     * Returns the title for this presentation.
	     */
	    public function get itemTitle(): String
	    {
	    	return _title;
	    }

	    /**
	     * Returns the publish date for this presentation in the form of a String.
	     */
	    public function get publishDate_string(): String
	    {
	    	return _publishDate;
	    }

	    /**
	     * Returns publish date for this presentation in the form of a date object.
	     */
	    public function get publishDate_time(): Date
	    {
	    	return new Date();
	    }

	    /**
		 * Constructor.  Loads presentation with xml data.
		 * 
		 * @param presData
		 */
	    public function PresentationModel(presData:XMLList): void
	    {
	    }

	    /**
	     * Sets the description for this presentation.
	     * 
	     * @param descStr
	     */
	    public function set itemDescription(descStr:String): void
	    {
	    }

	    /**
	     * Sets the title of this presentation.
	     * 
	     * @param titleStr
	     */
	    public function set itemTitle(titleStr:String): void
	    {
	    }

	    /**
	     * Sets the publish date for this presentation.  Date must be in the following
	     * format: yyyy-mm-dd
	     * 
	     * @param dateStr
	     */
	    public function set publishDate(dateStr:String): void
	    {
	    }

		

	}//end PresentationModel

}