///////////////////////////////////////////////////////////
//  BibleModel.as
//  ActionScript 3.0 Implementation of the Class BibleModel
//  Original author: Erica Stanley
///////////////////////////////////////////////////////////

package com.kingdomtools.mediaplayer.model
{
	import com.kingdomtools.mediaplayer.model.BookmarkModel;
	import com.kingdomtools.mediaplayer.model.SingletonEnforcer;

	import com.kingdomtools.mediaplayer.model.BookmarkModel;
	import com.kingdomtools.mediaplayer.model.SingletonEnforcer;
	/**
	 * This is the class that maintains all data relevant to the Bible navigation,
	 * including:  version, version data, current book, current chapter and an array
	 * of book marks.
	 * @author Erica Stanley
	 * @version 1.0
	 * @updated 01-Mar-2010 4:18:30 PM
	 */
	public class BibleModel
	{
	    /**
	     * Shared Object. Array containing active bookmarks specified by the user.
	     */
	    private var _bookmarks: Array;
	    /**
	     * Currently displayed book.
	     */
	    private var _currBook: String;
	    /**
	     * Currently displayed chapter.
	     */
	    private var _currChapter: int;
	    /**
	     * Single instance of the BibleModel class.
	     */
	    static private var _instance: BibleModel;
	    /**
	     * Current version of the Bible being displayed.
	     */
	    private var _version: String;
	    /**
	     * Current xml data associated with the current version being displayed.
	     */
	    private var _versionData: XML;

	    /**
		 * Adds bookmark object to Bible's bookmarks array.
		 * 
		 * @param bmkObject
		 */
	    public function addBookmark(bmkObject:BookmarkModel): void
	    {
	    }

	    /**
		 * Singleton Constructor.  Insures calling the constructor outside of this class
		 * will throw compiler error.
		 * 
		 * @param enforcer
		 */
	    public function BibleModel(enforcer:SingletonEnforcer): void
	    {
	    }

	    /**
	     * Allows access to the single instance of the PlaylistModel class.
	     */
	    static public function getInstance(): BibleModel
	    {
	    	return _instance;
	    }

	    /**
		 * Removes specified bookmark from Bible's bookmark array.
		 * 
		 * @param bmName
		 */
	    public function removeBookmark(bmName:String): void
	    {
	    }

	    /**
	     * Sets the version of the Bible to display and updates the version xml data.
	     * 
	     * @param versionName
	     */
	    public function set version(versionName:String): void
	    {
	    }

	    /**
	     * Updates version xml data for current version.
	     */
	    public function updateVersionData(): void
	    {
	    }

		
	}//end BibleModel

}