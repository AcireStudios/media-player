///////////////////////////////////////////////////////////
//  PlaylistModel.as
//  ActionScript 3.0 Implementation of the Class PlaylistModel
//  Original author: Erica Stanley
///////////////////////////////////////////////////////////

package com.kingdomtools.mediaplayer.model
{
	import com.kingdomtools.mediaplayer.model.FilterModel;
	import com.kingdomtools.mediaplayer.model.PresentationModel;

	import com.kingdomtools.mediaplayer.model.FilterModel;
	import com.kingdomtools.mediaplayer.model.PresentationModel;
	/**
	 * Singleton. The PlaylistModel is a collection of all available media items for
	 * the MediaPlayer.  It maintains the array of media items, active filters for the
	 * playlist (in the form of labels or tags) and the currently playing item.
	 * @author Erica Stanley
	 * @version 1.0
	 * @updated 01-Mar-2010 4:18:31 PM
	 */
	public class PlaylistModel
	{
	    /**
	     * String containing the name of the currently applied filter.  If no filter is
	     * currently being applied (entire play list is displayed), the value of this
	     * property is an empty string.
	     */
	    private var _currFilter: String;
	    /**
	     * Read only.  This is the index of the current presentation in the items array.
	     */
	    private var _currItemID: int;
	    /**
	     * This is an array of filters objects (labels or tags) supplied by the package
	     * rss.
	     */
	    private var _filters: Array;
	    /**
	     * Single instance of the PlaylistModel class.
	     */
	    static private var _instance: PlaylistModel;
	    /**
	     * The array of presentations for this play list
	     */
	    private var _items: Array;
	    /**
	     * Property indicating wheter the play list has completed loading items.
	     */
	    protected var loadComplete: Boolean;

	    /**
		 * 
		 * @param fName
		 */
	    public function addFilter(fName:String): void
	    {
	    }

	    /**
		 * 
		 * @param presentationXML
		 */
	    public function addPresentation(presentationXML:XMLList): void
	    {
	    }

	    /**
		 * Determines if a given filter has been created.
		 * 
		 * @param fName
		 */
	    public function filterCreated(fName:String): Boolean
	    {
	    	return true;
	    }

	    /**
	     * Allows access to the single instance of the PlaylistModel class.
	     */
	    static public function getInstance(): PlaylistModel
	    {
	    	return _instance;
	    }

	    /**
		 * 
		 * @param feedURL
		 */
	    public function loadPlaylist(feedURL:String): void
	    {
	    }

	    /**
		 * Singleton Constructor.  Insures calling the constructor outside of this class
		 * will throw compiler error.
		 * 
		 * @param enforcer
		 */
	    public function PlaylistModel(enforcer:SingletonEnforcer): void
	    {
	    }

	}//end PlaylistModel

}