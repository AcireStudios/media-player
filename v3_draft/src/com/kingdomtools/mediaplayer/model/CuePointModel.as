///////////////////////////////////////////////////////////
//  CuePointModel.as
//  ActionScript 3.0 Implementation of the Class CuePointModel
//  Original author: Erica Stanley
///////////////////////////////////////////////////////////

package com.kingdomtools.mediaplayer.model
{
	/**
	 * @author Erica Stanley
	 * @version 1.0
	 * @created 02-Feb-2010 01:59:22 AM
	 */
	public class CuePointModel
	{
	    /**
	     * This is the display time of the cue point in seconds.
	     */
	    private var _displayTime: Number;
	    /**
	     * Display type for this cue point.  Display can be either "text", "image" or
	     * "swf".
	     */
	    private var _displayType: String;
	    /**
	     * This is the source URL for this cue point.
	     */
	    private var _srcURL: String;

	    /**
		 * Constructor.  Sets the display type, display time and source URL for this cue
		 * point
		 * 
		 * @param dispTime
		 * @param srcURL
		 * @param dispType
		 */
	    public function CuePointModel(dispTime:Number, srcURL:String, dispType:String): void
	    {
	    }

	}//end CuePointModel

}