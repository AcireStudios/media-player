///////////////////////////////////////////////////////////
//  SkinModel.as
//  ActionScript 3.0 Implementation of the Class SkinModel
//  Original author: Erica Stanley
///////////////////////////////////////////////////////////

package com.kingdomtools.mediaplayer.model
{
	/**
	 * This class holds all of the skin information, including, the background color,
	 * the top color (for icons and text) , the theme and the URL for the logo image.
	 * @author Erica Stanley
	 * @version 1.0
	 * @updated 01-Mar-2010 4:18:31 PM
	 */
	public class SkinModel
	{
	    /**
	     * The URL for the logo image.
	     */
	    private var _logoURL: String;
	    /**
	     * URL to the specified theme.  The theme must be a swf designed specifically to
	     * work with the player.
	     */
	    private var _theme: String;
	    /**
	     * The rss-specified background color for the player.
	     */
	    private var bgColor: uint;
	    /**
	     * The rss specified color of the icons and text in the foreground of the player.
	     */
	    private var topColor:uint;

		public function SkinModel(){

		}

	}//end SkinModel

}