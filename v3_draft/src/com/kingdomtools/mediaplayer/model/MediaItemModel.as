///////////////////////////////////////////////////////////
//  MediaItemModel.as
//  ActionScript 3.0 Implementation of the Class MediaItemModel
//  Original author: Erica Stanley
///////////////////////////////////////////////////////////

package com.kingdomtools.mediaplayer.model
{
	/**
	 * @author Erica Stanley
	 * @version 1.0
	 * @created 02-Feb-2010 01:59:22 AM
	 */
	public class MediaItemModel
	{
	    /**
	     * The type for this media item.  Type may be either "audio" or "video".
	     */
	    private var _mediaType: String;
	    /**
	     * The URL for this media item.
	     */
	    private var _mediaURL: String;

	    /**
		 * Constructor.  Sets the  type and url for this media item.
		 * 
		 * @param mediaURL
		 * @param mediaType
		 */
	    public function MediaItemModel(mediaURL:String, mediaType:String): void
	    {
	    }

	}//end MediaItemModel

}