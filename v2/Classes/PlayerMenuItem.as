package {
	
	import flash.display.*;
	import flash.text.TextField;
	import flash.events.*;
	
	
	
	public class PlayerMenuItem extends MovieClip{
		
		//nested TextFields
		public var itemTitle:TextField;
		public var itemDescription:TextField;
		
		//nested clips
		public var divider:MovieClip;
		public var back:MovieClip;
		public var playBtn:SimpleButton;
		
		public var videoURL:String;
		
		public function PlayerMenuItem(){
			this.videoURL = "";
			
		}
		
		public function setData(dataObj:Object){
			this.itemTitle.text = dataObj.title;
			this.itemDescription.text = dataObj.description;
			this.videoURL = dataObj.guid;
			this.playBtn.addEventListener(MouseEvent.CLICK, gotoVideo);
			
		}
		
		public function gotoVideo(e:MouseEvent){
			
			var videoParent_ref = this.parent.parent.parent;
			trace("videoParent_ref -->"+ videoParent_ref);
			MovieClip(videoParent_ref).currentVideoURL = this.videoURL;
			MovieClip(videoParent_ref).hidePlaylist();
			MovieClip(videoParent_ref).mcVideoPlayer.bolLoaded = false;
			MovieClip(videoParent_ref).mcVideoPlayer.playVideo(true);
			
		}
		
		public function setWidth(w:Number){
			this.back.width = w;
			this.divider.width = w - 20;
			this.playBtn.x = w - 65;
			
			this.itemTitle.width = w - 85;
			this.itemDescription.width = w - 85;
			
		}
		
		
	}
	
	
}
	
	
