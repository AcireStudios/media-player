package {
	
	import flash.display.*;
	import flash.text.*;
	import flash.events.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	
	
	public class KTPlayerMenuItem extends MovieClip{
		
		private var kt:Namespace = new Namespace("http://www.kingdomtools.com/sermonplayer/schema");
		
		public var mediaType:String;
		public var videoURL:String;
		public var mediaAssets:Array = new Array();
		public var itemArtURL:String;
		
		public var playlistRef:MovieClip;
		public var playerRef:MovieClip;
		public var detailsRef:MovieClip;
		
		public var tags:Array = new Array();
		public var isSelected:Boolean = false;
		
		//nested TextFields
		public var itemTitle:TextField;
		public var itemDescription:TextField;
		
		//ui elements
		public var divider:MovieClip;
		public var back:MovieClip;
		public var playBtn:MovieClip;
		public var mediaIndicator:MovieClip;
		public var btn_downloadTranscript:MovieClip;
		public var btn_downloadMedia:MovieClip;
		public var btnItem:MovieClip;
		public var selectedState:MovieClip;
		
		public function KTPlayerMenuItem():void{
			this.videoURL = "";
			this.selectedState.visible = false;
			
			this.itemTitle.mouseEnabled = false;
			this.itemDescription.mouseEnabled = false;
			this.mediaIndicator.mouseEnabled = false;
			
		}
		
		public function setData(dataObj:Object):void{
			this.itemTitle.text = dataObj.title;
			truncateText(itemTitle);
			this.itemDescription.text = dataObj.description;
			this.itemArtURL = dataObj.kt::itemImage;
			this.mediaType =  dataObj.enclosure.@type;
			this.videoURL = dataObj.enclosure.@url;
			trace("--->Setting data for this media item: " + this.videoURL);
			
			if(mediaType == "video/flv"){
				this.mediaIndicator.gotoAndStop("video");
				//playerRef.gotoAndStop(1);
			}else{
				this.mediaIndicator.gotoAndStop("audio");
			}
			
			
			this.playBtn.buttonMode = true;
			this.playBtn.addEventListener(MouseEvent.CLICK, gotoVideo);
			
			
			this.btnItem.buttonMode = true;
			this.btnItem.addEventListener(MouseEvent.CLICK, showItemDetails);
			this.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			this.addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
			
			defineAssets(dataObj.kt::file);
			
			btn_downloadTranscript.buttonMode = true;
			btn_downloadTranscript.addEventListener(MouseEvent.CLICK, onDownloadPdf);
			
			btn_downloadTranscript.buttonMode = true;
			btn_downloadMedia.addEventListener(MouseEvent.CLICK, onDownloadMedia);
			
			var tagLength:int=0;
			
			if(dataObj.kt::verse[0] != null){
				tagLength = tags.push( new PlaylistItemTag());
				tags[tagLength-1].icon.gotoAndStop("verse");
				
				var verseStr:String = dataObj.kt::verse[0].@book +" "+dataObj.kt::verse[0].@chapter+":"+dataObj.kt::verse[0].@start;
				verseStr += (dataObj.kt::verse[0].@stop != "")? ("-"+dataObj.kt::verse[0].@stop): "";
				
				tags[tagLength-1].tagLabel.text = verseStr;
				tags[tagLength-1].category = new String("Scripture");
				
				tags[tagLength-1].buttonMode = true;
				tags[tagLength-1].mouseChildren = false;
				
				tags[tagLength-1].addEventListener(MouseEvent.CLICK, gotoVerse);
				
				addChild(tags[tagLength-1]);
				
			}
			
			if(dataObj.kt::customField[0] != null){
				for(var j:int = 0; j < dataObj.kt::customField.length(); j++){
					tagLength = tags.push( new PlaylistItemTag());
					
					if(dataObj.kt::customField[j].kt::label == "Speaker"){
						tags[tagLength-1].icon.gotoAndStop("speaker");
						tags[tagLength-1].category = new String("Speaker");
					}else{
						tags[tagLength-1].icon.gotoAndStop("custom");
						tags[tagLength-1].category = new String(dataObj.kt::customField[j].kt::label);
					}
					tags[tagLength-1].tagLabel.text = dataObj.kt::customField[j].kt::value;
					
					
					tags[tagLength-1].buttonMode = true;
					tags[tagLength-1].mouseChildren = false;
					
					tags[tagLength-1].addEventListener(MouseEvent.CLICK, gotoFilter);
					
					addChild(tags[tagLength-1]);
				}
				
			}
			//place tags
			var tags_startY:int = Math.round(itemDescription.y + itemDescription.height) + 10;
			var tags_newY:int = tags_startY;
			var tags_startX:int = itemDescription.x;
			
			var numRowItems:int = 2;
			var rowCount:int = Math.ceil(tags.length/ numRowItems);
			var i:int = 0;
			var k:int = 0
			
			/*for(var i:int = 0; i < tags.length; i++){
				if(k == 2) k = 0;*/
				
				for(var j:int = 0; j < rowCount; j++){
					tags_newY = tags_startY + j * (tags[0].height + 10);
					if(k == 2) k = 0;
					for(var k:int = 0; k < numRowItems; k++){
						tags[i].x = tags_startX + k * (tags[0].width + 10);
						tags[i].y = tags_newY;
						i++;
					}
				}
			//}
			
			
			setHeight( tags[ tags.length-1 ].y + tags[ tags.length-1 ].height + 10);
			
			
		}
		
		public function setType(rawType:String):void{
			
		}
		
		public function defineAssets(assets:XMLList){
			trace("assets--->\n"+assets);
			
			for(var i:int = 0; i < assets.length();	i++){
				mediaAssets[i] = new Object();
				mediaAssets[i].fileType = assets[i].@type;
				mediaAssets[i].fileURL = assets[i];
				
				trace("file---> "+mediaAssets[i].fileURL);
				
			}
			
			btn_downloadTranscript.visible = checkFileType("pdf");
			
			var media_dl_type:String ="";
			media_dl_type = checkFileType("video")? "video":"";
			media_dl_type = checkFileType("audio")? "audio":"";
			
			btn_downloadMedia.visible = (media_dl_type != "");
			btn_downloadMedia.gotoAndStop(media_dl_type);
			
		}
		
		protected function checkFileType(type:String):Boolean{
			var typeMatch:Boolean = false;
			
			for(var i:int = 0; i < mediaAssets.length;	i++){
				
				if(mediaAssets[i].fileType == type){
					typeMatch = true;
				}
			}
			
			return typeMatch;
			
			 
			
		}
		
		protected function getFileByType(type:String){
			for(var i:int = 0; i < mediaAssets.length; i++){
				if(mediaAssets[i].fileType == type){
					var request:URLRequest = new URLRequest(mediaAssets[i].fileURL);
					trace("Downloading file: " + mediaAssets[i].fileURL);
					try {
						navigateToURL(request, '_blank');
					} catch (e:Error) {
						trace("Error occurred!");
					}
					
				}
			}
			
		}
		
		protected function loadMediaDetails(){
			detailsRef.itemTitle.text = itemTitle.text;
			detailsRef.itemDescription.text = itemDescription.text;
			
			//remove old item art
			if(detailsRef.itemArt.numChildren > 0){
				detailsRef.itemArt.removeChildAt(0);
			}
			
			//load item art
			var ldr:Loader = new Loader();
			var urlReq:URLRequest = new URLRequest(itemArtURL);
			trace("loading image: " + itemArtURL);
			ldr.load(urlReq);
			ldr.contentLoaderInfo.addEventListener(Event.COMPLETE, imageCompleteHandler);
			detailsRef.itemArt.addChild(ldr);
			
			detailsRef.playBtn.buttonMode = true;
			detailsRef.playBtn.addEventListener(MouseEvent.CLICK, gotoVideo)
			MovieClip(playlistRef.parent).openMediaDetails(null);
			
			//remove old tags
			if(detailsRef.tags != null){
				for(var j:int = 0; j < detailsRef.tags.length; j++)
					detailsRef.removeChild(detailsRef.getChildByName( detailsRef.tags[j].name ));
			
			}	
			
			//load tags
			detailsRef.tags = new Array();
			for(var i:int = 0; i < Math.min(tags.length, 6); i++){
				detailsRef.tags[i] = new tag_details();
				detailsRef.tags[i].categoryName.text = tags[i].category +": ";
				detailsRef.tags[i].tagLabel.text = tags[i].tagLabel.text;
				
				detailsRef.tags[i].x = detailsRef.itemDescription.x;
				detailsRef.tags[i].y = 145 + i* detailsRef.tags[i].height;
				
				detailsRef.tags[i].name = "tag_" + detailsRef.tags[i].tagLabel.text;
				detailsRef.tags[i].mouseChildren = false;
				detailsRef.tags[i].buttonMode = true;
				
				if(tags[i].category == "Scripture"){
					detailsRef.tags[i].addEventListener(MouseEvent.CLICK, gotoVerse);
				}else{
					detailsRef.tags[i].addEventListener(MouseEvent.CLICK, gotoFilter);
				}
				
				var myFormat:TextFormat = new TextFormat();
				myFormat.font = "Arial";
				myFormat.bold = false;
				myFormat.color = 0x0000FF;
				myFormat.size = 12; 
				myFormat.underline = true;
				
				detailsRef.tags[i].tagLabel.setTextFormat( myFormat );
				
				
				detailsRef.addChild(detailsRef.tags[i]);
				
				
				
			}
			
		}
		
		protected function resizeMe(mc:MovieClip, maxW:Number, maxH:Number=0, constrainProportions:Boolean=true):void{
		    maxH = maxH == 0 ? maxW : maxH;
		    mc.width = maxW;
		    mc.height = maxH;
		    if (constrainProportions) {
			mc.scaleX < mc.scaleY ? mc.scaleY = mc.scaleX : mc.scaleX = mc.scaleY;
		    }
		}

		
		protected function imageCompleteHandler(e:Event):void  {
		    resizeMe(detailsRef.itemArt, 125, 240)
		}
		
		protected function onDownloadPdf(e:Event){
			getFileByType("pdf");
			
		}
		
		protected function onDownloadAudio(e:Event){
			getFileByType("mp3");
			
		}
		protected function onDownloadVideo(e:Event){
			getFileByType("video");
			
		}
		protected function onDownloadMedia(e:Event){
			
			var itemType:String = (mediaType == "video/flv")?"video":"audio"
			getFileByType(itemType);
			
		}
		
		public function onMouseOver(e:Event){
			this.selectedState.visible = true;
		}
		
		public function onMouseOut(e:Event){
			if(!isSelected)
				this.selectedState.visible = false;
		}
		
		public function showItemDetails(e:Event){
			
			playlistRef.unSelectAll();
			
			this.selectedState.visible = true;
			this.isSelected = true; 
			
			loadMediaDetails();
			
		}
		
		public function selectMe(){
			playerRef.strSource = this.videoURL;
			
			
			if(mediaType == "video/flv"){
				playerRef.itemArt.visible = false;
				playerRef.gotoAndStop(1);
				
				playerRef.bolLoaded = false;
				trace("1st Video ready: "+ playerRef.strSource);
				
				//playerRef.gotoAndStop(1);
				//playerRef.playVideo(true);
			}else{
				playerRef.itemArt.visible = true;
				playerRef.gotoAndStop(2);
				trace("1st Audio item ready: "+ playerRef.strSource);
				//playerRef.playAudio();
			}
		}
		
		public function gotoVideo(e:MouseEvent){
			
			playlistRef.unSelectAll();
			
			this.selectedState.visible = true;
			this.isSelected = true; 
			trace("videoParent_ref -->"+ playerRef);
			
			playerRef.strSource = this.videoURL;
			playlistRef.closeMenu();
			
			if(mediaType == "video/flv"){
				playerRef.itemArt.visible = false;
				playerRef.gotoAndStop(1);
				
				playerRef.bolLoaded = false;
				
				//playerRef.gotoAndStop(1);
				playerRef.playVideo(true);
			}else{
				playerRef.itemArt.visible = true;
				playerRef.gotoAndStop(2);
				playerRef.playAudio();
			}
			MovieClip(playlistRef.parent).mediaPlaying = true;
			MovieClip(playlistRef.parent).closeMediaDetails(null);
			
			
		}
		
		public function gotoVerse(e:MouseEvent){
			if( MovieClip(playlistRef.parent).itemOpen ){
				MovieClip(playlistRef.parent).closeMediaDetails(null);
			}
			playlistRef.bibleRef.showFilter(e.currentTarget.tagLabel.text);
			
		}
		
		public function gotoFilter(e:MouseEvent):void{
			if( MovieClip(playlistRef.parent).itemOpen ){
				MovieClip(playlistRef.parent).closeMediaDetails(null);
			}
			
			playlistRef.currentFilter = playlistRef.filterRef.getFilterByName(e.currentTarget.tagLabel.text);
			playlistRef.applyCurrentFilter();
			
		}
		
		public function truncateText($tf:TextField):void{
			trace("Max chars = "+$tf.maxChars);
			trace("Length = "+$tf.length);
			
			if($tf.length > 33){
				//var oldText:String = $tf.text;
				$tf.replaceText(30, $tf.length, "...")
				
			}
			
		}
		
		
		public function setWidth(w:Number){
			this.btnItem.width = w;
			this.divider.width = w - 20;
			this.playBtn.x = w - 65;
			
			this.itemTitle.width = w - 85;
			this.itemDescription.width = w - 85;
			
		}
		
		public function setHeight(h:Number){
			this.selectedState.height = h;
			this.btnItem.height = h
			this.divider.y = h;
			
		}
		
		
	}
	
	
}
	
	
