﻿package {
	
	import flash.display.*;
	import flash.text.TextField;
	
	import flash.system.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.events.*;
	
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	
	import com.flashloaded.ultimateScrollerAS3.ultimateScroller;
	
	
	import KTPlayerMenuItem;
		
	public class KTPlayerMenu extends MovieClip{
		                         
		public var menuRSS_URL:String = "";
		public var menuRSS:XML;
		public var menuRSSItems:XMLList = new XMLList();
		public var menuItems:Array = new Array();
		
		public var isLoaded:Boolean = false;
		public var currentVid:String;
		public var playerRef:MovieClip;
		public var bibleRef:MovieClip;
		public var filterRef:MovieClip;
		public var itemRef:MovieClip;
		public var currentFilter:Object;
		
		
		//ui elements
		public var videoList:MovieClip;
		public var back:MovieClip;
		public var scrollbar:ultimateScroller;
		public var divider_top:MovieClip;
		public var divider_bot:MovieClip;
		public var shadow:MovieClip;
		
		public var btnShowAll:MovieClip;
		public var btnShowAudio:SimpleButton;
		public var btnShowVideo:SimpleButton;
		public var btnShowTags:SimpleButton;
		
	
		
		//nested components
		public var menuScrollbar;
		
		public function KTPlayerMenu(){
			
			Security.allowDomain("*")
			//trace("class loaded");
			
			trace("Theme? "+ this.parent);
			
			
			this.currentVid = "";
			
			
			//for local testing purposes
			//loadMenuFeed();
			
			
			
			
			
		}
		
		public function loadMenuFeed(feedURL:String = "http://dev.kingdomtools.com/sermon_playlist/dev.kingdomtools.com/9"){
			var parentClass:Object = this;
			
			trace("--->default feed for playlist: " + feedURL);
			if(menuRSS_URL == "" || menuRSS_URL == null)
				menuRSS_URL = feedURL;
			
			var urlLoader = new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE,onXMLLoaded);
			urlLoader.load(new  URLRequest(menuRSS_URL));
			
			//set events
			
			setupButton(btnShowAll);
			btnShowAll.addEventListener(MouseEvent.CLICK, showAllFiles);
			btnShowVideo.addEventListener(MouseEvent.CLICK, showVideoFiles);
			btnShowAudio.addEventListener(MouseEvent.CLICK, showAudioFiles);
			btnShowTags.addEventListener(MouseEvent.CLICK, showTags);
			
		
		}
		
		public function setApplicationFeed(feedUrl:String){
			trace("--->In KTPlayerMenu, application feed: "+ feedUrl);
			
			menuRSS_URL = feedUrl;
			loadMenuFeed();
		}
		
		function onXMLLoaded(e:Event):void{
		       this.menuRSS =  new XML(e.target.data);
		       trace(this.menuRSS);
		       
		       this.loadMenuItems(388, 290);
		}
		
		public function loadMenuItems(w:Number = 400, h:Number = 300){
			
			//setSize(w, h);
			
			trace("Loading Items...");
			for(var i:int = 0; i < this.menuRSS..channel.item.length(); i++){
			
				trace(this.menuRSS..channel.item[i].title);
				this.menuItems[i] = new MovieClip();
				this.menuItems[i] = this.videoList.addChildAt(new KTPlayerMenuItem(), i);
				
				//place items
				if(i>0){
					this.menuItems[i].y = this.menuItems[(i-1)].y + this.menuItems[(i-1)].height;
				}
				//add data
				this.menuItems[i].playlistRef = this;
				this.menuItems[i].playerRef = this.playerRef;
				this.menuItems[i].detailsRef = this.itemRef;
				this.menuItems[i].setData(this.menuRSS..channel.item[i]);
				//this.menuItems[i].setWidth(w);
				
				
				
			}
			
			//this.btnShowAll.gotoAndStop("_down");
			
			trace("load first video: "+ this.menuRSS..channel.item[0].enclosure.@url);
			this.currentVid = this.menuRSS..channel.item[0].enclosure.@url;
			
			selectItem(0);
			//this.itemRef.btnBack.addEventListener(MouseEvent.CLICK,  MovieClip(parent).closeMediaDetails);
			
			this.isLoaded = true;
			
			
		}
		
		
		
		public function loadFilteredMenu( $filteredItems:Array ):void{
			//check to make sure items aren't null, then display
			for(var i:int = 0; i < $filteredItems.length; i++){
				if($filteredItems[i] != null){
					trace("Video url for filtered item: "+ $filteredItems[i].videoURL);
					
					//display item
					this.videoList.addChildAt($filteredItems[i], i);
					
					//place items
					if(i>0){
						$filteredItems[i].y = $filteredItems[(i-1)].y + $filteredItems[(i-1)].height;
					}else{
						$filteredItems[i].y = 0;
					}
					
				}else{
					trace("Item has no data.");
				}
			}
			
			
		}
		
		protected function unloadMenuItems(){ //remove items from display list, data should still be intact
			for(var j:int = this.videoList.numChildren -1; j > -1; j--){
				this.videoList.removeChildAt(j);
				//this.menuItems[j] = null;
			}
			
			
		}
		
		protected function clearMenuItems(){  //remove items and clear out data
			unloadMenuItems();
			this.menuItems = new Array();
			
		}
		
		public function applyCurrentFilter():void{
			unloadMenuItems();
			
			var filteredItems:Array = new Array();
			
			trace("Current filter is: "+this.currentFilter.name);
			
			for(var i:int =0; i<this.currentFilter.itemIndex.length; i++){
				trace("Video url for filtered item: "+ this.menuItems[ this.currentFilter.itemIndex[i] ].videoURL);
				filteredItems.push( this.menuItems[ this.currentFilter.itemIndex[i] ]);
			}
			loadFilteredMenu( filteredItems );
			
			openMenu();
		}
		
		public function setSize(w:Number, h:Number){
			this.back.width = w;
			this.back.height = h;
			
			
			if(this.isLoaded){
				for(var j:int = 0; j < this.menuRSS..channel.item.length(); j++){
					KTPlayerMenuItem(this.videoList.getChildAt(j)).setWidth(w);
				}
				
			}
			
		}
		
		public function selectItem(idx:int):void{
			this.menuItems[idx].isSelected = true;
			this.menuItems[idx].selectedState.visible = true;
			this.menuItems[idx].selectMe();
			
			
			this.currentVid = this.menuRSS..channel.item[idx].enclosure.@url;
			trace("selected item source url: "+this.currentVid);
			
		}
		
		public function unSelectAll(){
			for(var i:int = 0; i < this.menuItems.length; i++){
				this.menuItems[i].isSelected = false;
				this.menuItems[i].selectedState.visible = false;
			}
			
		}
		public function showAllFiles(e:MouseEvent){
			unloadMenuItems();
			
			
			
			for(var i:int = 0; i < this.menuItems.length; i++){
				//display item
				this.videoList.addChildAt(menuItems[i], i);
			
				//place items
				if(i>0){
					this.menuItems[i].y = this.menuItems[(i-1)].y + this.menuItems[(i-1)].height;
				}else{
					this.menuItems[i].y = 0;
				}
				
				
			}
		}
		
		public function showAudioFiles(e:MouseEvent){
			currentFilter = filterRef.getFilterByName("audio/mpeg");
			applyCurrentFilter();
		}
		
		public function showVideoFiles(e:MouseEvent){
			currentFilter = filterRef.getFilterByName("video/flv");
			applyCurrentFilter();
		}
		
		protected function showTags(e:MouseEvent):void{
			closeMenu();
			MovieClip(parent).openFilters(null);
		}
		
		public function setupButton(btn:MovieClip):void {
	    	trace("Setting up "+ btn.name);
		    btn.buttonMode = true;
		    btn.mouseChildren = false;
		}
		
		public function closeMenu(){
			MovieClip(parent).closePlaylist(null);
			
		}
		
		
		public function openMenu(){
			if( !MovieClip(parent).playlistOpen )
			MovieClip(parent).openPlayList(null);
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}