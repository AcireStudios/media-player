﻿package {
	
	import flash.display.*;
	import flash.text.TextField;
	
	import flash.system.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.events.Event;
	
	import fl.transitions.Tween;
	import fl.transitions.easing.*;
	
	
	import PlayerMenuItem;
	import tiledMenuBkg;
	
	public class PlayerMenu extends MovieClip{
		
		public var menuRSS:XML;
		public var menuRSSItems:XMLList = new XMLList();
		public var menuItems:Array = new Array();
		
		public var isLoaded:Boolean = false;
		public var currentVid:String;
		//nested TextFields
		public var subheader:TextField;
		
		//nested clips
		public var videoList:MovieClip;
		public var back:MovieClip;
		public var subheader_borders:MovieClip;
		public var tiledBg:MovieClip;
		public var tileMask:MovieClip;
		//public var tiledMenuBkg:MovieClip;
	
		
		//nested components
		public var menuScrollbar;
		
		public function PlayerMenu(){
			
			Security.allowDomain("*")
			//trace("class loaded");
			
			
			this.currentVid = "";
			//loadMenuFeed("http://acirefx.com/testing/speedtv/speed_speedreport_podcast.xml");
			loadMenuFeed("http://podcasts.speedtv.com/speed_2_wheel_tuesdays_podcast.xml");
			
			
		}
		
		public function loadMenuFeed(feedURL:String){
			var parentClass:Object = this;
			
			var urlLoader = new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE,onXMLLoaded);
			urlLoader.load(new  URLRequest(feedURL));
			trace(feedURL);
		
		}
		
		function onXMLLoaded(e:Event):void{
		       this.menuRSS =  new XML(e.target.data);
		       trace(this.menuRSS);
		       
		       this.loadMenuItems(stage.stageWidth, stage.stageHeight-80);
		}
		
		public function loadMenuItems(w:Number, h:Number){
			
			setSize(w, h);
			tileClipHorizontally(w);
			
			for(var i:int = 0; i < this.menuRSS..channel.item.length(); i++){
			
				trace(this.menuRSS..channel.item[i].title);
				this.menuItems[i] = new MovieClip();
				this.menuItems[i] = this.videoList.addChildAt(new PlayerMenuItem(), i);
				
				//place items
				if(i>0){
					this.menuItems[i].y = this.menuItems[(i-1)].y + this.menuItems[(i-1)].height;
				}
				//add data
				this.menuItems[i].setData(this.menuRSS..channel.item[i]);
				this.menuItems[i].setWidth(w);
				
			}
			//tileClipHorizontally(w);
			//this.menuScrollbar.x = w - 20;
			//this.menuScrollbar.height = h - this.subheader_borders.height-5;
			trace("load first video: "+ this.menuRSS..channel.item[0].enclosure.@url);
			this.currentVid = this.menuRSS..channel.item[0].enclosure.@url;
			this.isLoaded = true;
			
			
		}
		
		/*function unloadMenuItems(){
			for(var j:int = 0; j < this.menuItems.length; j++){
				this.videoList.removeChildAt(j);
				
			}
			
		}*/
		
		public function setSize(w:Number, h:Number){
			this.back.width = w;
			this.back.height = h;
			this.subheader_borders.width = w;
			this.tileMask.width = w;
			this.tileMask.height = h;
			this.menuScrollbar.x = w - 20;
			this.menuScrollbar.height = h - this.subheader_borders.height-5;
			//tileClipHorizontally(w);
			
			if(this.isLoaded){
				for(var j:int = 0; j < this.menuRSS..channel.item.length(); j++){
					SpeedTV_MenuItem(this.videoList.getChildAt(j)).setWidth(w);
				}
				
			}
			
		}
		
		
		public function tileClipHorizontally(w:Number){
			var tiles:Array = new Array();
				tiles[0] = new MovieClip();
				tiles[0] = this.tiledBg.addChildAt(new tiledMenuBkg(), 0);
			var repeat_x = Math.ceil(w / tiles[0].width);
			trace(repeat_x);
			
			for(var i:int = 1; i < repeat_x; i++){
				tiles[i] = new MovieClip();
				tiles[i] = this.tiledBg.addChildAt(new tiledMenuBkg(), i);
				
				//place tiles
				tiles[i].x = tiles[(i-1)].x + tiles[(i-1)].width;
			}
			
			//this.tileMask.width = w;
		}
		
		public function closeMenu(){
			var hideTween = new Tween(this, "x", Regular.easeOut, 0, -this.width, .75, true);
			MovieClip(this.parent).menuOpen=false;
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}